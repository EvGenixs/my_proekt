package com.example.redalert.valutess;

import android.app.ListActivity;
import android.os.Bundle;
import android.widget.TextView;

import com.example.redalert.R;

import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class valutes extends ListActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_2);
        final List<Valute> listValute;

        Retrofit retrofit = new retrofit2.Retrofit.Builder()
                .baseUrl("http://www.cbr.ru/")
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .build();
        server11 api = retrofit.create(server11.class);
        api.getData("28/10/2019").enqueue(new Callback<ValCurs>() {
            @Override
            public void onResponse(Call<ValCurs> call, Response<ValCurs> response) {
                if (response.isSuccessful()) {
                    setDataToList(response.body().getList());
                } else {
                    TextView tvv = findViewById(R.id.title_banks);
                    tvv.setText(response.message());
                }
            }

            @Override
            public void onFailure(Call <ValCurs> call, Throwable t) {
                TextView tv = findViewById(R.id.title_banks);
                tv.setText("Fail");
            }
        });
    }
    void setDataToList(List<Valute> listv){
        setListAdapter(new MyClassAdapter(this,listv));
    }
}
