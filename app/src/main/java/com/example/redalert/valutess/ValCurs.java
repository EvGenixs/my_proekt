package com.example.redalert.valutess;

import org.simpleframework.xml.Attribute;
import org.simpleframework.xml.ElementList;
import org.simpleframework.xml.Root;

import java.util.List;

@Root(name = "ValCurs")
public class ValCurs {
    @ElementList(inline=true)
    public List<Valute> valu;

    @Attribute (name = "Date")
    public String _Date;

    @Attribute (name = "name")
    public String _name;

    public List<Valute> getList(){
        return this.valu;
    }

}
