package com.example.redalert;

import android.app.AlertDialog;
import android.app.Dialog;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.Toolbar;

import androidx.fragment.app.DialogFragment;

import com.example.redalert.service_auth.Auth_Interceptor;
import com.example.redalert.service_auth.Authed_page;
import com.example.redalert.service_auth.LoginService;
import com.example.redalert.service_auth.User;

import okhttp3.Credentials;
import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MyDialogF extends DialogFragment {
    View view;
        @Override
        public Dialog onCreateDialog(final Bundle savedInstanceState){
            final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            LayoutInflater inflater = getActivity().getLayoutInflater();
            this.view = inflater.inflate(R.layout.dialog_signin,null);
            builder.setView(view);
            Button bt1 = view.findViewById(R.id.b1);
            bt1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    login();
                }
            });
            Button bt2 = view.findViewById(R.id.b2);
            bt2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    dismiss();
                }
            });

            return builder.create();
        }
    public void login(){
            EditText edUser = this.view.findViewById(R.id.et1);
            EditText edPass = this.view.findViewById(R.id.et2);
            String user = String.valueOf(edUser.getText());
            String password = String.valueOf(edPass.getText());
            if (!TextUtils.isEmpty(user)&& !TextUtils.isEmpty(password)){
                /*Чтобы генерировать учетные данные для Basic authentication, вы можете использовать
                класс OkHttps Credentials с его базовым (String, String) методом. Метод принимает
                имя пользователя и пароль и возвращает учетные данные аутентификации для Http Basic схемы.
                */
                        String authInf = Credentials.basic(user,password);
                Retrofit.Builder br = new Retrofit.Builder()
                        .baseUrl("http://intelligent-system.online/")
                        .addConverterFactory(GsonConverterFactory.create());
                OkHttpClient.Builder okhttp = new OkHttpClient.Builder();
                Auth_Interceptor interceptor = new Auth_Interceptor(authInf);
                okhttp.addInterceptor(interceptor);
                br.client(okhttp.build());
                Retrofit retrofit = br.build();
                LoginService api =  retrofit.create(LoginService.class);
                Call<User> call = api.auth();
                call.enqueue(new Callback<User>() {
                    @Override
                    public void onResponse(Call<User> call, Response<User> response) {
                        if (response.isSuccessful()) {
                            SharedPreferences sharedPreferences = getContext().getSharedPreferences("app",0);
                            SharedPreferences.Editor editor = sharedPreferences.edit();
                            editor.putString("token",response.body().tuken);
                            editor.commit();
                            Intent intent = new Intent(getActivity(), Authed_page.class);
                            startActivity(intent);
                        }
                    }
                    @Override
                    public void onFailure(Call<User> call, Throwable t) {
                        Toast toast = Toast.makeText(getContext(),"Ошибка с авторизацией, проверьте подключение к Интернету",Toast.LENGTH_LONG);
                        toast.show();
                    }
                });

            }
    }
}

