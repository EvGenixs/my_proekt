package com.example.redalert.addressees;

import com.google.gson.annotations.SerializedName;


public class Dom {
        @SerializedName("address")
        private String ad;
        @SerializedName("timeend")
        private String t_E;
        @SerializedName("timestart")
        private String t_S;
        @SerializedName("type")
        public String tip;


        public String getAd(){
            return ad;
        }
        public String getT_E(){
            return t_E;
        }
        public String getT_S() {
            return t_S;
        }
        public String getTip(){
            return tip;
        }

}
