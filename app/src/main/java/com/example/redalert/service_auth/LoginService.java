package com.example.redalert.service_auth;

import retrofit2.Call;
import retrofit2.http.POST;

public interface LoginService {
    @POST("api/tokens")
    Call<User> auth();
}
