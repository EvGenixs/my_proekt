package com.example.redalert.service_auth;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.example.redalert.R;

import okhttp3.OkHttpClient;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class Authed_page extends AppCompatActivity {
    public static final String API_BASE_URL = "http://intelligent-system.online/";
    private static OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
    private static Retrofit.Builder builder =
            new Retrofit.Builder()
                    .baseUrl(API_BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create());
    private static Retrofit retrofit = builder.build();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.authed_page_design);
        SharedPreferences sharedPreferences = getSharedPreferences("app",0);
        String token = sharedPreferences.getString("token","fail");
        Auth_Interceptor interceptor = new Auth_Interceptor("Bearer " + token);
        httpClient.addInterceptor(interceptor);
        builder.client(httpClient.build());
        retrofit = builder.build();
        Skip_check api = retrofit.create(Skip_check.class);
        api.getSkip(new skip("95")).enqueue(new Callback<skip>() {
            @Override
            public void onResponse(Call<skip> call, Response<skip> response) {
                if(response.isSuccessful()){
                    Toast toast = Toast.makeText(getBaseContext(),response.body().ID,Toast.LENGTH_SHORT);
                    toast.show();
                }
                else {
                    Toast.makeText(getBaseContext(),response.message(),Toast.LENGTH_SHORT).show();
                }
            }
            @Override
            public void onFailure(Call<skip> call, Throwable t) {
                Toast toast = Toast.makeText(getBaseContext(),"loser",Toast.LENGTH_SHORT);
                toast.show();
            }
        });


    }
}
